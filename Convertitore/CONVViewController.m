//
//  CONVViewController.m
//  Convertitore
//
//  Created by Claudio Ruggeri on 18/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import "CONVViewController.h"
#import "CONVSecondController.h"

@interface CONVViewController ()

@end

@implementation CONVViewController
@synthesize logButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *createURL = [NSString stringWithFormat:@"http://currencies.apps.grandtrunk.net/currencies"];
	NSURL * currenciesPath = [NSURL URLWithString:createURL];
	NSLog(@"%@", createURL);
	//NSData *XMLData   = [NSData dataWithContentsOfURL:XMLPath];
	NSString *currenciesData = [[NSString alloc]initWithContentsOfURL: currenciesPath encoding:NSUTF8StringEncoding error:nil];
    self.currenciesArray = [currenciesData componentsSeparatedByString:@"\n"];
	//NSLog(@"%@",HTMLentity);
	if (currenciesData == nil) {
		NSLog(@" currencies non trovate!"); 
	}else {
        NSLog(@"%@",currenciesData);
        NSLog(@"%i",[self.currenciesArray count]);
    };
  //  self.view = self.tableView;
    logButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    logButton.frame = CGRectMake(120, 120, 80, 30);
    [logButton setTitle:@"Open" forState:UIControlStateNormal];
    [logButton addTarget:self action:@selector(openPage) forControlEvents:UIControlEventTouchUpInside];
    //  self.logView.backgroundColor = [UIColor whiteColor];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    logTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenBound.size.width, screenBound.size.height-44) style:UITableViewStyleGrouped];
    logTable.delegate = self;
    logTable.dataSource = self;
    logTable.scrollEnabled = NO;
    [self.view addSubview:logTable];
    [self.view addSubview:logButton];

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
};


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.textLabel.text = @"User";
        self.userField = [[UITextField alloc]initWithFrame:CGRectMake(100, 0, 185, 44)];
        self.userField.placeholder = @"Insert username";
        self.userField.textAlignment  = NSTextAlignmentRight;
        self.userField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.userField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.userField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.userField.delegate = self;
        //     self.amountField.backgroundColor = [UIColor lightGrayColor];
        [cell addSubview:self.userField];

    }else{
        cell.textLabel.text = @"Password";
        self.passField = [[UITextField alloc]initWithFrame:CGRectMake(100, 0, 185, 44)];
        self.passField.placeholder = @"Insert password";
        self.passField.textAlignment  = NSTextAlignmentRight;
        self.passField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.passField.secureTextEntry = YES;
        self.passField.delegate = self;
        //     self.amountField.backgroundColor = [UIColor lightGrayColor];
        [cell addSubview:self.passField];

    }
    
    return cell;
};

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"premuto: %i",indexPath.row);
};

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
 
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [self openPage];
    return YES;
};

- (void)openPage{
    CONVSecondController *secondView = [[CONVSecondController alloc] init];
    if ([self.userField.text isEqualToString:@"test"]&&[self.passField.text isEqualToString:@"test"]) {
        secondView.currencies = self.currenciesArray;
        [self.navigationController pushViewController:secondView animated:true];
        self.userField.text = @"";
        self.passField.text = @"";
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Username or password incorrect" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    //textLabel.text = [NSString stringWithFormat:@"Button Pressed!"];
};

@end
