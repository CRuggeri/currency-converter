//
//  CONVViewController.h
//  Convertitore
//
//  Created by Claudio Ruggeri on 18/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CONVViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    UIButton *logButton;
    UITableView * logTable;
    
}

@property (strong,nonatomic) UITextField *userField;
@property (strong,nonatomic) UITextField *passField;
@property (strong,nonatomic) NSArray *currenciesArray;
@property (strong,nonatomic) UIButton *logButton;
@end
