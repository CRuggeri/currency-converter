//
//  main.m
//  Convertitore
//
//  Created by MacMini1 on 18/02/13.
//  Copyright (c) 2013 MacMini1. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CONVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CONVAppDelegate class]));
    }
}
