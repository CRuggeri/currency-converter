//
//  CONVSecondController.m
//  Convertitore
//
//  Created by Claudio Ruggeri on 18/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import "CONVSecondController.h"
#import "CONVCurrenciesController.h"

@interface CONVSecondController ()

@end

@implementation CONVSecondController



- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%i",[self.currencies count]);
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    converterTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenBound.size.width, screenBound.size.height-44) style:UITableViewStyleGrouped];
    converterTable.delegate = self;
    converterTable.dataSource = self;
    [self.view addSubview:converterTable];
    self.fromCurrency = [[NSString alloc] init];
    self.toCurrency = [[NSString alloc] init];
    conversionRate = 0;
     self.navigationItem.title = @"Converter!";
    converterTable.scrollEnabled = NO;
    UIButton *changeButton = [[UIButton alloc] initWithFrame:CGRectMake(140, 38, 32, 32)];
    [changeButton setBackgroundImage:[UIImage imageNamed:@"up_down_arrow.png"] forState:UIControlStateNormal];
    [changeButton addTarget:self action:@selector(changeField) forControlEvents:UIControlEventTouchUpInside];
    [converterTable addSubview:changeButton];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.amountField becomeFirstResponder];
    [self.amountField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    if (![self.amountField.text isEqualToString:@""]){
        float value = [self.amountField.text floatValue]*conversionRate;
        self.resultLabel.text = [NSString stringWithFormat:@"%10.2f",value];
    }
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStylePlain target:nil action:nil];
    
};

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	

    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0: {
            cell.textLabel.text = @"From:";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.fromLabel  = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 185, 44)];
            self.fromLabel.text = @"";
            self.fromLabel.backgroundColor = [UIColor clearColor];
            self.fromLabel.textAlignment  = NSTextAlignmentRight;
            [cell addSubview:self.fromLabel];
        }
        break;
        case 1:{
            cell.textLabel.text = @"To:";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.toLabel  = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 185, 44)];
            self.toLabel.text = @"";
            self.toLabel.backgroundColor = [UIColor clearColor];
            self.toLabel.textAlignment  = NSTextAlignmentRight;
            [cell addSubview:self.toLabel];
        }
        break;
        case 2:
            cell.textLabel.text = @"Amount:";
            self.amountField = [[UITextField alloc]initWithFrame:CGRectMake(100, 0, 185, 44)];
            self.amountField.placeholder = @"Insert Amount";
            self.amountField.textAlignment  = NSTextAlignmentRight;
            self.amountField.delegate = self;
            self.amountField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
       //     self.amountField.backgroundColor = [UIColor lightGrayColor];
            self.amountField.keyboardType = UIKeyboardTypeDecimalPad;
            [cell addSubview:self.amountField];

            break;
        case 3:{
            cell.textLabel.text = @"Result:";
            self.resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 185, 44)];
            self.resultLabel.text = @"0.00";
            self.resultLabel.backgroundColor = [UIColor clearColor];
            self.resultLabel.textAlignment  = NSTextAlignmentRight;
            [cell addSubview:self.resultLabel];
        }
        break;
        default:
            break;
    }
    // Configure the cell...
    
    return cell;
}

- (void)openPageCurrencie:(NSString *)code {
    CONVCurrenciesController *currenciesView = [[CONVCurrenciesController alloc] init];
    currenciesView.delegate = self;
    currenciesView.currencies = self.currencies;
    currenciesView.currencyCode = code;
    [self.navigationController pushViewController:currenciesView animated:true];
};

- (void)getCurrencyFrom:(NSString *)fromCur to:(NSString *)toCur{
    NSLog(@"TO: %@",self.toCurrency);
    if ((![self.fromCurrency isEqualToString:@""])&&(![self.toCurrency isEqualToString:@""])) {
        NSString *currencyURL = [NSString stringWithFormat:@"http://currencies.apps.grandtrunk.net/getlatest/%@/%@",fromCur,toCur];
        NSURL * currencyPath = [NSURL URLWithString:currencyURL];
        NSLog(@"%@", currencyPath);
        //NSData *XMLData   = [NSData dataWithContentsOfURL:XMLPath];
        NSString *currencyData = [[NSString alloc]initWithContentsOfURL: currencyPath encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"%.10f", [currencyData floatValue]);
        conversionRate = [currencyData floatValue];
        //   self.resultLabel.text = currencyData;
    };
};

- (void)changeField {
    NSString *swap = self.fromCurrency;
    self.fromCurrency = self.toCurrency;
    self.toCurrency = swap;
    self.fromLabel.text = self.toLabel.text;
    self.toLabel.text = swap;
    [self getCurrencyFrom:self.fromCurrency to:self.toCurrency];
    float value = [self.amountField.text floatValue]*conversionRate;
    self.resultLabel.text = [NSString stringWithFormat:@"%10.2f",value];
  //  [converterTable reloadData];
};

- (void)currenciesViewControllerDidFinish:(CONVCurrenciesController *)currenciesController{
    NSLog(@"Ritorna: %@",currenciesController.currencyCode);
    if(isFrom){
        self.fromCurrency = currenciesController.currencyCode;
        self.fromLabel.text = currenciesController.currencyCode;
    }else {
        self.toCurrency = currenciesController.currencyCode;
        self.toLabel.text = currenciesController.currencyCode;
    }
    [self getCurrencyFrom:self.fromCurrency to:self.toCurrency];
    NSLog(@"FROM: %@",self.fromCurrency);
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //l'if non è indispensabile, perché c'è solo un textField, ma l'if verifica che sia quello giusto se ce ne fosse più di uno
    
    [textField resignFirstResponder];
    return YES;
};

- (void)textFieldDidChange:(id)sender {
    UITextField *textField = (UITextField *)sender;
    NSLog(@"Ratio di conversione: %.10f",conversionRate);
    NSLog(@"TextField: %.2f",[textField.text floatValue]);
    float value = [textField.text floatValue]*conversionRate;
    self.resultLabel.text = [NSString stringWithFormat:@"%10.2f",value];
};

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"premuto: %i",indexPath.row);
    switch (indexPath.row) {
        case 0:
            isFrom = YES;
            [self openPageCurrencie:self.fromCurrency];
            break;
        case 1:
             isFrom = NO;
            [self openPageCurrencie:self.toCurrency];
            break;
        default:
            break;
    };

    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
