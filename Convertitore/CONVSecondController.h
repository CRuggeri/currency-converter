//
//  CONVSecondController.h
//  Convertitore
//
//  Created by Claudio Ruggeri on 18/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CONVCurrenciesController.h"

@interface CONVSecondController : UIViewController <UITableViewDelegate,UITableViewDataSource,CONVCurrenciesControllerDelegate,UITextFieldDelegate>
{
    UITableView* converterTable;
    BOOL isFrom;
    float conversionRate;
}

@property (strong,nonatomic) UILabel *fromLabel;
@property (strong,nonatomic) UILabel *toLabel;
@property (strong,nonatomic) UITextField * amountField;
@property (strong,nonatomic) UILabel *resultLabel;
@property (weak, nonatomic) NSArray *currencies;
@property (strong, nonatomic) NSString *fromCurrency;
@property (strong, nonatomic) NSString *toCurrency;

@end
