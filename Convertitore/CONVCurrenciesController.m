//
//  CONVCurrenciesController.m
//  Convertitore
//
//  Created by Claudio Ruggeri on 19/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import "CONVCurrenciesController.h"


@interface CONVCurrenciesController ()

@end

@implementation CONVCurrenciesController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%i",[self.currencies count]);
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    self.navigationItem.title = @"From Currency";
    currenciesTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenBound.size.width, screenBound.size.height-44) style:UITableViewStyleGrouped];
    currenciesTable.delegate = self;
    currenciesTable.dataSource = self;
   
    [self.view addSubview:currenciesTable];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSLog(@"Disappear");
    [self.delegate currenciesViewControllerDidFinish:self];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.currencies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    };
    
    cell.textLabel.text = [self.currencies objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([cell.textLabel.text isEqualToString:self.currencyCode]) {
        self.indexCheck = indexPath;
    };
    
       // Configure the cell...
    if([self.indexCheck isEqual:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSLog(@" %i",self.indexCheck.row);
   // if(indexCheck.row != indexPath.row){
        if (self.indexCheck) {
            UITableViewCell *cellToUnCheck = [tableView cellForRowAtIndexPath:self.indexCheck];
            cellToUnCheck.accessoryType = UITableViewCellAccessoryNone;
        };

        UITableViewCell *cellToCheck = [tableView cellForRowAtIndexPath:indexPath];
        cellToCheck.accessoryType = UITableViewCellAccessoryCheckmark;
        self.indexCheck = indexPath;
        self.currencyCode = [NSString stringWithString:cellToCheck.textLabel.text];
        [self.navigationController popViewControllerAnimated:YES];    
    //};
   // NSLog(@" %i",self.indexCheck.row);
};

@end
