//
//  CONVCurrenciesController.h
//  Convertitore
//
//  Created by Claudio Ruggeri on 19/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CONVCurrenciesControllerDelegate;

@interface CONVCurrenciesController : UIViewController <UITableViewDelegate,UITableViewDataSource>{
    UITableView* currenciesTable;
    
  //  NSIndexPath* indexCheck;
}

@property (weak,nonatomic) id<CONVCurrenciesControllerDelegate> delegate;
@property (weak, nonatomic) NSArray *currencies;
@property (strong, nonatomic) NSIndexPath* indexCheck;
@property (strong, nonatomic) NSString *currencyCode;
@end

@protocol CONVCurrenciesControllerDelegate  <NSObject>

- (void)currenciesViewControllerDidFinish:(CONVCurrenciesController *)currenciesController;

@end