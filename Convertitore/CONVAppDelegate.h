//
//  CONVAppDelegate.h
//  Convertitore
//
//  Created by Claudio Ruggeri on 18/02/13.
//  Copyright (c) 2013 Claudio Ruggeri. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CONVViewController;

@interface CONVAppDelegate : UIResponder <UIApplicationDelegate,UINavigationBarDelegate>{
    UIWindow *window;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *viewController;

@end
